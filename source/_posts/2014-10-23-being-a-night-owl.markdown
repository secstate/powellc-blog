---
layout: post
title: "being a night owl"
date: 2014-10-23 01:16
comments: true
categories: 
---

It's 0116 EDT and I'm still awake. This has become a trend recently. No matter
how diligent I try to be about wrapping things up and going to be early, I
always seem to wind up going to bed later and later until it's way past the mid
point of the night and I am feeling guilty.

That's an interesting emotion. Why should I feel guilty? Because I wont be very
useful in the morning (or visible at all until around 0900)? Because my staying
up late means more work for my wife in the morning with the kids? No. I need to
stop feeling guilty. But it's hard. Not least of all because our culture frowns
on staying up late, and there's research to show it'as actually unhealthy too.

<!-- more --> 

I just finished reading the book [The End of
Night](http://www.hachettebookgroup.com/titles/paul-bogard/the-end-of-night/9780316182904/)
by Paul Bogard. He makes some bold claims in the book, mostly backed up with
references, but the boldest is that flooding yourself with light long after the
sun has gone down has long term health consequences, from sleep disorders to
cancer. Even as I read it, I can't avoid staying up late.

When I go to bed at a resonable time, I still wind up getting up late. Sure I
can get up at 6, but for the first hour I'm a zombie. Whereas if I stay up
until 2 and wake up at 9, I'm ready to go, and I had the benefit of nearly 3
hours of alone time to get personal projects done.

But I'm still on the fence. Especially with daylight-savings time coming up,
and training for a marathon. Running the morning when it gets cold out is
really important. The sun is coming up on you then, rather than setting on you
when you run in the afternoon. So perhaps I'll try again to get to bed early
over the next few days. I suppose first I'll have to convince myself there's
nothing much to do at night. And that's just a lie.
