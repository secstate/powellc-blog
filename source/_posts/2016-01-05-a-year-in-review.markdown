---
layout: post
title: "A year in review"
date: 2016-01-05 15:53
comments: true
categories: 
---

Welcome to tomorrow ... today! I can hardly remember what happend last month,
let alone January of last year, but let's give it a shot, eh?

We're still raising pigs, kids and occassionaly vegetables in Castine, Maine.
Colin is still happily working as a software developer for Five Q. Emma
continues to search for the craft project that will make her rich (so many
schemes at the moment). Jane started kindergarten in the fall. Silas started
talking back this summer. We took a family trip to Disney World. Sam finished
up her semester at Coastal Studies for Girls. Peter kicked the crap out of his
homeschool curriculum. Colin also, completed three marathons, and two half-marathons
and registered for his first ultra marathon (now he's just showing off). Emma
is pregnant with our third biological child, who shall be a surprise but named
either Asa or Abigail. Whew!

<!-- more -->

In other news, Peter and Sam moved out of our house in the middle of 2015. The
circumstances were less than happy, but the change was made for the benefit of
us all. Mostly Emma and Colin found themselves woefully unable to provide the
environment that they needed to be successful and as a result no one was happy
with the outcomes. We initially left the situation a bit open, but the longer
and happier they remained in their new household, the more we took on the role
of supporters, rather than parents.

We still all live in the same town, and their guardian is one of our best
friends so we see them often, and even rang in 2016 with them, albeit with all
the South Georgians at 9 PM EST, rather than midnight (just try keeping a 5 and
2 year old up for new year's eve). So, major rough patch to the year, but
things seem to be better for all inovled now.

There was, obviously, a lot more going on in 2015. But that was the big stuff.
For everything else, Colin occassionally posts on Facebook or Twitter so look
him up there.
