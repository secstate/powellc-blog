---
layout: post
title: "A daring return"
date: 2014-04-22 14:40
categories: [meta]
---

In one magnificent stroke, I announce my intention to blog more. That's right
... I've not been bloggin' for a bit. Couldn't you tell? What? Nevermind.
That's not important right now.

What is important is that I'll be updating folks on what's going on in the
lives of us Timberwyckians. All the ups and downs of trying to be the best
father I can be, while juggling being a half-way decent software developer and
farmer. Whew. That's a lot of stuff!

<!-- More -->

The gist of this return is that I'm tired of folks not knowing what's going on
way out here in Maine. Despite the fact that my the Older Sister and her family
are moving out here, there are still a decent number of diaspora all over the
country that would like to know what's going on here. And while the Timberwyck
Farm blog is a pretty could touch-point for much of that. This will aim to be a
bit more personal.