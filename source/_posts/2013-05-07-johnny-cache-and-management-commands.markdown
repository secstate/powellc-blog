---
layout: post
title: "Johnny Cache &amp; Management Commands"
date: 2013-05-07 21:18
comments: true
categories: [Django,Cache,Management,Gist]
---

Discovered a pretty huge caveat to using Johnny Cache to speed up Django ORM queries.

Johnny Cache is a really great library that can speed up Django sites that are slowing down with large joins and complex data models. Django has a built in ORM caching mechanism, but Johnny Cache takes it one step further.

<!-- more -->

That said, there are some monsters in the undiscovered ocean. One of the big ones is, I feel, not prominent enough: [enabling cache validation and invaldiation when running mangaement commands](http://pythonhosted.org/johnny-cache/queryset_cache.html#using-with-scripts-management-commands-asynchronous-workers-and-the-shell).

You must enable Johnny Cache some where in your project that gets run everytime manage.py is used. The top level init.py file is a good candidate in most projects. If you don't do that, management commands will leave your views all screwed up and you'll wonder why things that should be published are not.

I learned the hard way.
