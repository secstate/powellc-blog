---
layout: post
title: "Snopocalypse Redux"
date: 2013-02-09 13:46
comments: true
categories: 
---

Well, it happened. Kind of wondered if we were all out of snow for the year, 
as the extended forcast for the past few weeks hinted at flurries or showers 
but nothing else. Than, wham! A classic Nor'easter comes up an wallops us good.

{% img http://onecardinal.smugmug.com/Nature/Snowmageddon-2013/i-T4pF98d/0/X2/DSC_1975-X2.jpg 700 %}

<!-- more --> 

And this is even the sort of storm that the old-timers up here in Maine can't
say anything about. 1978, that's the last storm this size, and before that it
was even longer. Of course, we have wing-plows and all manner of other modern
conveniences, but still.

The most challenging part for us has been making sure the animals are taken
care of. The pigs, especially, are still on open pasture, and while the NE wind
direction has been forgiving to a large degree, their hutches still are not as
cozy as we would have liked.

Still, we're stugged in, and if that's the worst part of the storm, no harm, no
foul. As I sit here and write the snow is coming up past the windows in drifts.
Where there are no drifts we've got at least 18 inches, probably considerably
more, but it's hard to accurately measure.

{% img http://onecardinal.smugmug.com/Nature/Snowmageddon-2013/i-fWT9mTx/0/X2/DSC_2008-X2.jpg %}

{% img http://onecardinal.smugmug.com/Nature/Snowmageddon-2013/i-2f2Lz8C/0/X2/DSC_2006-X2.jpg %}
