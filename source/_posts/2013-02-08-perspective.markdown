---
layout: post
title: "Perspective"
date: 2013-02-08 22:39
comments: true
categories: 
---

There are a great many trials and tribulations to life. Somedays you don't feel well. Others it seems like nobody, not even your partner, is on the same page with you.

Life throws us curveballs, and anyone who believes that they have their routine down so well that they can't be interrupted is not being honest with themselves.
