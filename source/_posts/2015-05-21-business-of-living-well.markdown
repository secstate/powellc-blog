---
layout: post
title: "The Business of Living Well"
date: 2015-05-21 18:25
comments: true
categories: 
---

There was a news-hole-filler story today about how 19 predictions Bill Gates
made for 2014 in 1999 have mostly come true. In one prediction, he
prognosticates that by now we'd have devices that would provide us with up to
date flight information, market quotes and news. Something about his list rang
true with the way I was raised, but not the way I live now.

I grew up in the suburbs of Chicago. When the day was done, and my dad was home
from work, he would park himself down in front of the TV playing some sort of
stock market review show about how things had gone. I grew up thinking that
this was all really important news gathering. And to a lot of professionals in
certain markets it is. But as an amateur stock market investor who's occupation
was a doctor, the market was really more of a fetish.

What I failed to realize then is that far less important that what the Blue
Chips did on Mach 19, 1999, was what *I was doing* on March 19, 1999

<!-- more -->

I appreciate that may sound conceited, or grasping. But what I mean is that
what my dad was up to watching the stock market roll along was the work of
millions of people doing something. Bubbles and stock fraud aside, the best way
to ensure growth in your company's valuation (and if it's public, it's stock
value) is to run the company well, increase the value it provides to it's
customers and treat your employees right. That's not a promise, even for a
company with great stock growth. But the only way you get a four percent
average return over a century is by working efficiently.

Those who ride the market hoping to make a sound investment should either make
their living helping people make sound fiscal decisions with their savings, or
are mildly delusional. Real estate has always, and will always be a better
guarantor of return on investment over extremely long periods of time. And land
has another benefit, it can be developed, improved and made into a business on
it's own merits, rather than by holding on the coattails of hot-shot business
owners.

None of this is to say that I advocate avoiding the stock market, or that there
isn't a sound personal fiscal policy in market investing. But watching the
"markets" day in and day out for growth is a bit like buying an apartment
building and then standing in the doorway when you get done with your work day
just to appreciate that it has people living in it.

That gets us back to Bill's post. The predictions Bill Gates made about where
we'd be with technology was from such a twisted and narrow view of the world.
Flights, market quotes and "news" will just come to us! What he failed to see,
and I believe was part and parcel of the world I grew up in, was the growth in
the web as a learning medium, a place where you can get answers to rather
mudane, but important-to-you questions.

Even more than that, digital devices and the web have allowed many people to
decentralize their life, telecommute, and live where they want, instead of
where Sears tells them they need to for that promotion to middle management.

As someone who builds websites for a living and also lives on and runs a small
organic farm, the ability of my digital device and the web to allow me to do
what I love, while living where I love and with the hobbies I love, *that*
makes all the difference.

