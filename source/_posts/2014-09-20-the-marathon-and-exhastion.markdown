---
layout: post
title: "the marathon and exhaustion"
date: 2014-09-20 10:12
comments: true
categories: 
---

I'm going to be running in my first marathon running race on October 5. I have
to admit to being both nervous and skeptical about how the experience will be.

In training for the race, I've been increaing my distances on the weekend
fairly slowly, while ostensibly keeping up a usual training routine the rest of
the week (illness and busyiness aside). My conclusion so far: it's not worth
it. I'm sure there are people out there who are built for long distance
running. And there may even be people who enjoy it despite not being built for
it. Me, I love the half-marathon distance. I can do it, I feel good when I'm
done and I can be competitive at it.

So far, the longest run I've ever done is 17.5 miles. That's quite an
achievement, but it left me feeling sore for days. From everything I've read,
20 miles is really the point at which you stop gaining anything from a run and
start just wearing down your joints and muscles. I'd wager that I got close to
that point last weekend, and it wasn't fun at all.

<!-- more --> 

And here we come to crux of my issue. I run for fun, and I run becasue of the
great health benefits it imparts on the rest of my life. If I'm feeling tired,
or getting bogged down by work on the computer, running perks me up. It helps
move things along in my body, and keeps my resting heart rate nice and low so I
at least don't have to worry about my general cardio health.

When I started competitive running last year, I was ambitious and looking for
the next big challenge. What I've discovered is that there is ALWAYS a next big
challenge. Even if I were to complete a marathon in under 4 hours, I could
shoot for sub 3:30, or even a Boston qualifying time. Then I could train for
ultra marathons. It's a never ending race.

Personally, I'd rather run because it makes me feel good, than run with a wild
ambition to "beat" myself and go farther. I know there are some great,
ambitious runners who do not agree with me, and that's totally cool. I'll see
you at the your half-marathon "trainer" :)
