---
layout: post
title: "On clearing trees and education&nbsp;Hurdles"
date: 2014-04-28 09:52
comments: true
categories: ['education', 'wood', 'cutting']
author: Colin Powell
---

While it wasn't on my list of things that have to get done, Emma suggested we
start clearing up some junky pine and spruce trees that are in various states
of dying along the road in front of the house. The appearence of them certainly
doesn't help the house, and it could actively hurt if a wind storm brings one
down.

So we set to work taking down the first one and I nearly killed myself. Even a
short, 10-foot pine tree has plenty of weight to  it. And when someone gives a
sharp tug on a come-along rope, and the tree bounces backwards and begins to
come down the opposite direction from where you expected it, all you can do is
jump.

I laid out the opposite direction, winding up prone on the ground with a pine
tree lying next to me. Needless to say, I was a little gun-shy when it came to
the second tree of the day.

<!-- more -->

Emma, always willing to reach out for help, offered to call a friend who has a
lot of experience felling trees in all kinds of crazy positions. He stopped by
on his way home, and gave me some great pointers about how to cut the wedge and
walked me through the back cut. [1] It was fabulous. The tree came down right
where we wanted it to, didn't crush anything, and I was able to remain standing
as it came down.

#### Bringing down barriers to education

While I obviously didn't know all that much about felling trees, and could use
some helpful tips, I knew enough to be dangerous. We recently opted to do a
home-school curriculum with our eldest son. This was not an easy decision and
it took some mustering to find the time, and at the end came down to his
behavior in school getting worse and the value proposition of spending time in
the school building approaching zero.

What we discovered was that a great many people know just enough about
education to be dangerous. Trapped in the pedagogy of what education is
supposed to look like, our school was spending a lot time doing things mostly
right, and then jumping out of the way (sending him home) when that back up
that really looked pretty good caused the tree to jump back on them.

This is in no way meant as a criticism of individual people at our small town
school, who are all very skilled at what they do. But in the presence of changing
physiology, diets, and cultural expectations, 19th century ideas of education
seem to be doing more harm than good. Combine that with live experiments done
in a non-scientific manner (project-based learning introduced without proper
feedback mechanisms, or online tools being used as babysitters), and you have a
recipe for an education that leaves something to be desired.

So now our son spends some time in school and some time out of school. We're
not 100% home-schooling, though technically we control the curriculum and it's
filed with the state. The freedom that this opens up is for his day to be more
organically structured and better overseen when necssary. And like most things,
we'll take a measured approach and see how he's doing at various intervals, but
already his mood has improved and he's more willing to do things that he ought
to do as opposed to always looking for things he wants to do, which is
refreshing and encouraging.


[1] Bascially, he suggested the wedge be cut at a
60 degree angle up, with the horizontal cut out at the top of the wedge. The
back cut, then was kept almost perpendicular to the tree, with a slight angle
downward, towards the point where the two wedge cuts come together. This, he
explained, would stop the tree from rolling as it bent and ensure it fell where
you were hoping it would. He also pointed out that my saw was really dull and
that that can make the cuts time consuming and more dangerous, go figure.
