---
layout: post
title: "planning for trouble"
date: 2015-01-22 09:55
comments: true
categories: 
---

"Never live beyond your means," my father would say. The human calculus that
leads folks to impart their own brand of wisdom in their offspring and mentees
is beyond me. And I deeply respect folks who have demonstrated the success of
their wisdom via life success.

But as I get older and embark on adventures of fatherhood, profession and
happiness of my own, I'm finding that repurposing wisdom is far better than
blindly following it. For the above quote, I'm finding it difficult to imagine
how you can possibly avoid living beyond your means. 

<!-- more --> 

In a purely financial sense, that means spending less than you make. Which is a
fabulous piece of advice. Money lenders only exist so long as they stand to
benefit from an eventual payout. When that payout gets bleak, lenders disappear
and you are left owing more than you can pay, which we call bankruptcy. At this
point you most assuredly will be living beyond your means. Life will become difficult.

But my realization is this: "With every sip off the fountain of life we're
living beyond our means." The blessing to be alive is given to us for only a
brief time, and while we're in our mortal coil everything, even our financial
security, is only possible through an infinitely complex web of existence that
betrays all our best planning.

Briefly, we're all in this together. 

Once we've understood that and dispensed with shallow, Rand-ian philosphy of 
isolationism, we can better address **how** we express our gratitude for the 
means of our living. If we can't reduce our lives to the simple maxim of not
living beyond our means, indeed, if our only choice is to live beyond our
means, then the only path to a happy, productive life is to show
consistent gratitude to those around us.

For me, that means living a stoic lifestyle, but that doesn't mean emotionless. It
means a measured and reasoned response to the difficulties of life. We must
acknowledge, like Marcus Aurelius or Epicetus before us, that we only control
our response to the events of our lives, and that a negative response rarely 
provides a positive resolution to an event.

My pledge, as I seek to embrace an honest stoic philosophic life is to respond
rationally and positively to the events that happen to me in my life. A big
part of fullfilling that pledge is taking part in an activity which is often
called "planning for adversity." Essentially, one meditates on a ficticious
worry, considering what a stoic response would be and allowing the negative
emotional response to pass by you into oblivion.

I've built a tool to help me keep to this goal, as well:
[Plan for Adversity](https://www.planadversity.com)

My hope and belief is that through stocisim and adversity planning, I will be
better able to repay gratitude with more gratitude, improving the social
condition of those around me whom I benefit from so much.
