---
layout: post
title: "Universal Scale"
date: 2015-03-01 08:25
comments: true
categories: 
---

Scifi authors and dreaming scientists believe that humans will one day find a
way to tame the great distances between stars. While not the crux of the novel,
Stanislaw Lem's [Fiasco](https://en.wikipedia.org/wiki/Fiasco_(novel)) presents 
a humbling observation about what long-distance star travel would actually 
amount to: isolation. Unless you bring a civilization worth of people with you,
a journey out of the Solar System, regardless of the mechanics of it (worm 
hole, warp drive, conventional engines) would leave those on the ship more or
less alone for the rest of their lives. Baring the discovery of a unique property 
of cosmological constructs like black holes which we understand very little of,
the nature of time and relativity means once you've travelled far enough from
your home planet, you can never be sure what you'll return home to.

<!-- more -->

Even more than isolation for the humans involved in such a flight, unless we
were content with launching ourselves into the cosmos to study inorganic
chemicals on distant plants, our likely destination would be another
civlization. Again, in Lem's novel, the problems of relativity hound you no
matter where you go. If we discovered a civilization orbiting one of the closer
stars in our galaxy, the time required to actually transit to the civlization
would make an encounter exceedingly difficult.

Dinosaurs ruled the Terran ecological system for an order of magnitude longer
than humans currently do. Yet, the time span of 100 million years would go by
in a few months traveling at or near the speed of light. By the time you
arrived on a distant planet, there could easily be multiple large extinction
events removing nearly all trace of an advanced civilzation. 

These thought exercises are yet founded on what we currently know. The future
is so vast and full of possibility that it has made Jules Vern look foolish
even as he got a lot things right. So who am I to say we'll never be able to
transit the cosmos efficiently? Yet, I can't help but feel humbled by the sheer
size of our known universe. It's almost as though it wasn't built for creatures
with our sense of time. As though humans on Earth, a small rocky ball swinging
around a fairly minor hydrogen reactor in the backwater of an average galaxy
were mostly unimportant to the machinations of stellar physics.

And yet, I dream; I aspire to build, create and sometimes destroy. And I am
not alone in these urges. It feels, often, like there's something else driving
me, and my fellow humans to ever grander visions of our future, and what we can
do and where we can live. Where do our dreams come from?
