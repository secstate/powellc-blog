---
layout: post
title: "Mini Habits"
date: 2015-04-25 08:25
comments: true
categories: 
---

For the past year or so I've been experimenting on and off with a technique
that I picked up from Stephen Guise via his website:
[https://minihabits.com/](https://minihabits.com/). The long and short of his
technique lies in making a decision about something you'd like to do or learn
and start with the easiest possible step towards it and do it everyday.

So if you want to write more, endeavor to write two sentences everyday. Not a
blog post, not two pages. Just two sentences. The logic is that if you make it
too easy to fail, the success of achieving your goals will spur you do a little
more each time until you find you're doing way more than you expected and, far
from a chore, it's actually become a habit; something that you are compelled to do.

Guise goes through a lot more detail and I highly encourage you to check out
his ebook on ths subject, but the question is, did it work for me?

<!-- more -->

I'll admit I was skeptical at first. The idea of moving the bar low enough that
you could not fail sounded like advice from your soccer coach in middle school
who was also your P.E. and biology teacher. "Just set your expectations low!""

Far from it, the goal with mini habits, or stupid-easy habits, as I like to
call them, is that you are purposely tricking your brain into making a good
habit. Rather than something like smoking which is a bad habit, and fairly easy
to aquire and hard to break, something like practicing guitar 30 minutes per
day is a hard habit to develop, but usually good ... unless it leads to you
start a cover band that gets a taste of success and you decide to divorce your
spouse and take it on the road, making $200 a month once you pay your booking
agent.

So the question Guise asked was, how do you make the good habit easy to
develop? Or at least, easier. His lesson is to break it down into chunks that
are achievable each and everyday. And I can honestly say it works amazingly.

I've had a guitar sitting in the corner of my house that I got as a Christmas
present when I was 14. I had never gotten around to learning because I would
feel inspired for a week, practice an hour every couple of days and then get
tired and stop.

Now I just noodle around for 15 miniutes each day, doing some basic chord
progressions or scales and strumming out a few tunes. What began a month ago
with me setting a timer, has become a habit that I literally look forward to in
the evening. No timer, just an internal clock that says it's been at least 15
minutes. Maybe less, but usually more. And that's the power of mini habits. You
make the goal so easy to achieve that you are constantly providing your brain
with positive feedback.

I've done the same trick with diary writing. Started out with one or two
sentences per day, the bare minimum of an entry. At this point I can't sit down
without writing a few paragraphs. But if I've had a crazy day? One sentence
still suffices to stroke my ego and let me know I haven't fallen off the wagon.

It's really powerful. Give it a shot.
