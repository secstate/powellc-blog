---
layout: post
title: "The cold breaks"
date: 2014-01-05 01:46
comments: true
categories: Weather
author: Colin Powell
---

Anyone who's ever said "cold is cold" hasn't spent much time below zero. I've spent plenty of time there myself, especially when I was a college student in Wisconsin, but for some reason, as both a farmer and homeowner, this cold snap felt different. Perhaps it was the fact that our car's blower motor burned out a week before, or maybe it's just watching the oil level drop precipitously when the degree-day measurment is 60+ (that's indoor temp minus outdoor temp).

{% img right http://onecardinal.smugmug.com/Events/Maine-Ice-Storm-2013/i-5SpCzTH/0/X2/DSC_3169-X2.jpg 500 500 %}

In any case, having to lug water out to the pigs in the pasture was an exercise in keeping warm. While it's still true that moving around outside keeps the blood going, when the mercury hits -5 or so and the wind chill is worse, your hands go numb pretty quickly, regardless. But the cold snap has passed, for now. We're finally getting back to our regularly scheduled, maritime moderate winter temperatures. Doing the animal chores with the temps in the 20s I actually built up a sweat. That's more like it.

<!-- more -->

It's been an interesting year so far, weather-wise. While it's hard to claim any year as substantially different, better or worse than another, this winter has so far been notable for not having a lot of thaws after major storms. During this last cold snap, we actually had 5-6 inches of snow accumulate on one of the coldest nights. Granted, the snow was some of the finest powder I've ever seen in Maine, but still. Very little thawing going on around here. Which begs the question of what mud season will be like.
