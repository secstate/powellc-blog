---
layout: post
title: "Marathon running as test but not destruction"
date: 2015-10-20 23:53
comments: true
categories: 
---

A little over a week ago I finished the Chicago Marathon. Me along with about
40,000 of my closest friends. But seriously, that's about a factor of 50 larger
than my current home village here on the coast of Maine. By my own calculation
I finished fifth out of about 25 fellow runners in Maine, or around 7,555th in
the whole thing. My finish was a little bit of a disappointment, but I refuse
to be too hard on myself anytime I run more than 22 miles.

Here's the thing: People are always writing or opinining about why they run
26.2 miles. I've done the same thing, and the answers are usually pretty trite.
But the thing that gets my goat more than any of the lame excuses for pushing
yourself really hard is when folks talk about finishing the distance as though
it were going to destroy them. Or as if they were about to punish their body
with the full distance of a marathon.

<!-- more -->

If you're going into it with that mindset, I'd rather not run along side you.
There definitely exists a wall, and it's different for everyone. No one ever
said a marathon was easy. But if your body seriously screams at you to stop,
just stop. Or at the very least, don't tell everyone about how your body was
ripping itself asunder as you hit that 20 mile wall but you pushed on through
the pain.

You knew it would be hard and it was. If it was easy, everyone would do it. If
your body is seriously hurt after a marathon run, consider training more and
competing at shorter distances. But please, for your health and the sanity of
folks who can complete the marathon distance while remaining upright and not
walking a significant portion of it, focus on improving. I recommend Jack
Daniels' guide to running within your ability but competitively in his Daniels'
Running Formula.
